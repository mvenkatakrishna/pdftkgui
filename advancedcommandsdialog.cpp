/**************************************************************************
 *  This file is part of PDFtkGUI.                                        *
 *                                                                        *
 *  PDFtkGUI is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  PDFtkGUI is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.       *
 **************************************************************************/

/*
 * Author: Mahesh Venkata Krishna (https://bitbucket.org/mvenkatakrishna/)
 */

#include "advancedcommandsdialog.h"
#include "ui_advancedcommandsdialog.h"

#include <QProcess>

AdvancedCommandsDialog::AdvancedCommandsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdvancedCommandsDialog)
{
    ui->setupUi(this);

    m_command = "pdftk ";
    ui->cmdEdit->setText(m_command);
}

AdvancedCommandsDialog::~AdvancedCommandsDialog()
{
    delete ui;
}

void AdvancedCommandsDialog::setInputFile(QString fName)
{
    // Use the name provided if it is not empty
    QString tmp = fName.replace(" ", "");
    if(tmp != "")
    {
        m_command += fName;
        ui->cmdEdit->setText(m_command);
    }
}

void AdvancedCommandsDialog::on_pushButton_clicked()
{
    QProcess proc;

    // Get the command from the text edit, run the command and then show the process output
    proc.start(ui->cmdEdit->text());
    proc.waitForFinished(-1);
    QString currStdout = proc.readAllStandardOutput();
    QString currStderr = proc.readAllStandardError();

    ui->stdoutEdit->setPlainText(currStdout);
    ui->stderrEdit->setPlainText(currStderr);
}
