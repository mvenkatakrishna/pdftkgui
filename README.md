# PDFtkGUI #

PDFtkGUI is a Qt-based GUI wrapper for the popular [PDFtk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) toolkit.

So far, I have tested it only on Ubuntu 17.10.
### Usage ###

#### Standrad usage ####
Standards usage means the rather simple usecases one comes across in the day-to-day scenarios, such as stitching pdfs, splitting them, rotating selected pages, collating scanned pages from two different files, etc. This program is actually designed with this usage in mind.

To do any of these operations, just click on "Add Files" button and in the dialog, select one or more pdf files. Then, for each file, you can specify which pages need to be on the output (in the "Used Pages"). For example, if you need to join pages 1 to 3 in A.pdf and pages 3-4 on B.pdf, then in the 3rd column, simply enter 1-3 for row with A.pdf and 3-4 in the row of B.pdf. Clicking on the 4th column will set rotation (Possible values: "None", "90deg Clock-Wise", "90deg Counter-Clock-Wise", "180deg"). You can also use the word "end" to denote the last page, eg., "1-end".

To create a pdf with alternating pages from A.pdf and B.pdf, put the two files in the list, set the ranges as before and check the shuffle box. Note however, that in case more than two files are present, only the first two will be used.

Once the input files are configured, select the output file name and path and then click on "Create PDF" to create the output.

#### Advanced usage ####
This progam is not really designed for relatively advanced usage of PDFtk, like working with passwords, adding watermark, extracting pdf info etc. However, if, for whatever reason you need to use these features, as of now, you need to select the "Advanced Usage" button and enter the PDFtk commands in the dialog that pops up. In the future versions, more support for these features may be added.


For more information on PDFtk usage and options, please refer to the [PDFtk documentation](https://www.pdflabs.com/docs/pdftk-man-page/).

### Compilation ###

You need the Qt 5.10 libraries and gcc to compile. Pick the source files and build!

### Contribution guidelines ###

This project is in alpha stage. To contribute, please contact me by mail first!

### Acknowledgements ###
I am thankful to Sid Steward who wrote [PDFtk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/). The toolkit has been of great use to me personally so often that I wanted to come up with a GUI for linux users, which led to PDFtkGUI.

### Contact ###

Mail me at: vkmaheshbhat@gmail.com
