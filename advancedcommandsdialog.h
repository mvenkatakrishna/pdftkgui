/**************************************************************************
 *  This file is part of PDFtkGUI.                                        *
 *                                                                        *
 *  PDFtkGUI is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  PDFtkGUI is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.       *
 **************************************************************************/

/*
 * Author: Mahesh Venkata Krishna (https://bitbucket.org/mvenkatakrishna/)
 */

/*
* This dialog provides the advanced usage feature.
*/

#ifndef ADVANCEDCOMMANDSDIALOG_H
#define ADVANCEDCOMMANDSDIALOG_H

#include <QDialog>

namespace Ui {
class AdvancedCommandsDialog;
}

class AdvancedCommandsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AdvancedCommandsDialog(QWidget *parent = 0);
    ~AdvancedCommandsDialog();

    // This is a function to let the mainwindow set a filename if it is already available. Not used now.
    void setInputFile(QString fName);

private slots:
    void on_pushButton_clicked();

private:
    Ui::AdvancedCommandsDialog *ui;

    QString m_command;
};

#endif // ADVANCEDCOMMANDSDIALOG_H
