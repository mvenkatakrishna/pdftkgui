/**************************************************************************
 *  This file is part of PDFtkGUI.                                        *
 *                                                                        *
 *  PDFtkGUI is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  PDFtkGUI is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.       *
 **************************************************************************/

/*
 * Author: Mahesh Venkata Krishna (https://bitbucket.org/mvenkatakrishna/)
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "helpdialog.h"

#include <QRegExp>
#include <QMessageBox>
#include <QProcess>
#include <QDir>
#include <QFileDialog>
#include <QSet>

#include <set>

#include <QDebug>

// Function to show the messagebox to report error/warning/success.
void throwErrorMsg(errId err, QString errOut)
{
    QString msg = "";
    switch(err)
    {
        case PDFTK_NOT_FOUND:
            msg = "Can't locate PDFtk! Please make sure you have installed PDFtk correctly...";
            break;
        case PDFTK_SUCCESS:
            msg = "PDF successfully generated!";
            break;
        case PDFTK_FAILED:
            msg = "PDFtk failed! Following is the error:\n"
                  "------------------------------------------------------------"
                  "\n" + errOut;
            break;
        case EMPTY_FILELIST:
            msg = "File list is empty!";
            break;
        case OFNAME_WRONG:
            msg = "Output file field is empty!";
            break;
        case TOO_FEW_TO_SHUFFLE:
            msg = "File list has only " + errOut + " elements. Need two files to shuffle!";
            break;
        case MORE_THAN_TWO_FILES:
            msg = "There are " + errOut + " files in the list. Will use only the first two to shuffle!";
            break;
        case INVALID_ERR:
            msg = "Invalid Error!";
            break;
    }

    QMessageBox msgBox;
    msgBox.setText(msg);
    msgBox.exec();
}

// Function to get the number of pages in the input file.
// Uses PDFtk's dump_data option to get the info.
QString getNoOfPages(QString fName)
{
    QProcess proc;
    proc.start("pdftk " + fName + " dump_data");
    proc.waitForFinished(-1);
    QString currStdout = proc.readAllStandardOutput();

    // Catch the number of pages field
    QRegExp ppnum("NumberOfPages: (\\d+)");
    if(ppnum.indexIn(currStdout) > -1)
    {
        return ppnum.cap(1);
    }
    return QString();
}

// Mapping files to PDFtk-style ids like "A", "B" etc. After 26, "AA", "AB"...
QString getDocStringFromIndex(int idx)
{
    if(idx < 0)
        return QString();
    else if(idx < 26)
    {
        char c = 'A';
        c += idx;
        return QString(c);
    }
    else if(idx < 702)
    {
        char c1 = 'A';
        char c2 = 'A';
        c1 += ((idx/26) -1);
        c2 += idx%26;
        return QString("%1%2").arg(c1).arg(c2);
    }

    return QString();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Handling clicks in the rotation column
    connect(ui->fileTable, SIGNAL(itemClicked(QTableWidgetItem*)),
            this, SLOT(onThirdColumnClicked(QTableWidgetItem*)));

    ui->fileTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);

    // Check whether pdftk exists. If not nothing will work!
    QProcess proc;
    proc.start("pdftk");
    proc.waitForFinished(-1);
    QString proc_stdout = proc.readAllStandardOutput();
    QString proc_stderr = proc.readAllStandardError();
    // Pdftk output always contains this word when it exists
    if(proc_stdout.contains("SYNOPSIS"))
    {
        m_pdftkExists = true;
    }
    else
    {
        m_pdftkExists = false;
        throwErrorMsg(PDFTK_NOT_FOUND, QString());
    }

    // Setup the dialogs
    m_hlpDlg = new HelpDialog();
    m_advDlg = new AdvancedCommandsDialog();

    m_command = "";
}

MainWindow::~MainWindow()
{
    delete m_hlpDlg;
    delete m_advDlg;
    delete ui;
}

void MainWindow::setupCommand()
{
    // List of file names
    QStringList fileList;
    // List of page numbers to go into the output
    QStringList pages;
    // List of rotation settings for each document
    QStringList rot;

    // Iterate over all the rows in the input files table
    for(int i = 0; i < ui->fileTable->rowCount(); ++i)
    {
        QTableWidgetItem* it0 = ui->fileTable->item(i, 0);
        // Sanity check
        QString it0Simple = it0->text().replace(" ", "");
        if(it0Simple != "")
        {
            QTableWidgetItem* it1 = ui->fileTable->item(i, 1);
            int numpp = it1->text().toInt();

            QTableWidgetItem* it2 = ui->fileTable->item(i, 2);
            // Sanity check
            QString it1Simple = it1->text().replace(" ", "");
            if((it1Simple == "") || !numpp )
            {
                continue;
            }

            // Regexp to catch the page numbers out of the table entries
            // Can be of format ex. 1-5 or 1-end
            QRegExp ppNr("(\\d+)-(\\d+|end)");

            int start = 1;
            int end = numpp;

            if((ppNr.indexIn(it2->text()) > -1) && (ppNr.captureCount() >= 2))
            {

                start = ppNr.cap(1).toInt();
                if(!it2->text().endsWith("end"))
                {
                    end = ppNr.cap(2).toInt();
                }
            } // end if it2

            // If everything is fine, append the file name and page ranges to the lists,
            // and then handle the rotation info.
            if(start && end && (start <= end))
            {
                fileList.append(it0->text());
                pages.append(QString("%1-%2").arg(start).arg(end));

                QTableWidgetItem* it3 = ui->fileTable->item(i, 3);
                if(it3->text() == "90 deg CW")
                {
                    rot.append("right");
                }
                else if(it3->text() == "90 deg CCW")
                {
                    rot.append("left");
                }
                else if(it3->text() == "180 deg")
                {
                    rot.append("down");
                }
                else
                {
                    rot.append("");
                }
            } // end if start-end
        } // end if it0
    } // end for i

    // If file list is empty, nothing to do
    if(fileList.length() == 0)
    {
        return;
    }

    // If shuffle command is to be used, then make sure there are at least 2 files, and if not,
    // tell the user and return. If there are more than 2, tell the user that we use the first 2.
    // Also remove the remaining from the list.
    if(ui->shfleBox->isChecked())
    {
        if(fileList.length() < 2)
        {
            throwErrorMsg(TOO_FEW_TO_SHUFFLE,
                          QString("%1").arg(fileList.length()));
            return;
        }
        else if(fileList.length() > 2)
        {
            throwErrorMsg(MORE_THAN_TWO_FILES,
                          QString("%1").arg(fileList.length()));
            while(fileList.length() > 2)
            {
                fileList.removeLast();
                pages.removeLast();
                rot.removeLast();
            }
        }
    }

    // Setup the input file list
    for(int f = 0; f < fileList.length(); ++f)
    {
        m_command += getDocStringFromIndex(f);
        m_command += "=";
        m_command += fileList[f];
        m_command += " ";
    }

    // Poll the shuffle check box state
    if(ui->shfleBox->isChecked())
    {
        m_command += "shuffle ";
    }
    else
    {
        m_command += "cat ";
    }

    // Form the command string finally
    for(int f = 0; f < fileList.length(); ++f)
    {
        m_command += getDocStringFromIndex(f);
        m_command += pages[f];
        m_command += rot[f];
        m_command += " ";
    }
}

void MainWindow::on_addBtn_clicked()
{
    if(!m_pdftkExists)
    {
        throwErrorMsg(PDFTK_NOT_FOUND, QString());
        return;
    }

    QStringList files = QFileDialog::getOpenFileNames(this,
                            "Select one or more files to open",
                            QDir::homePath(), "*.pdf");

    // For each new file, get the file info and add a row to the table
    for(int i = 0; i < files.length(); ++i)
    {
        QString nPages = getNoOfPages(files[i]);
        if(nPages == QString())
        {
            continue;
        }

        // Insert the new files at the end and add the items and fill them with the info
        ui->fileTable->insertRow(ui->fileTable->rowCount());
        for (int c=0; c < ui->fileTable->columnCount(); ++c)
        {
            ui->fileTable->setItem( ui->fileTable->rowCount()-1, c,
                                    new QTableWidgetItem(QString()));
        }

        ui->fileTable->item(ui->fileTable->rowCount()-1, 0)->setText(files[i]);
        ui->fileTable->item(ui->fileTable->rowCount()-1, 1)->setText(nPages);
        ui->fileTable->item(ui->fileTable->rowCount()-1, 2)->setText("1-end");
        ui->fileTable->item(ui->fileTable->rowCount()-1, 3)->setText("None");
    }
}

void MainWindow::on_clrBtn_clicked()
{
    // Clear the table
    for (int r=0; r < ui->fileTable->rowCount(); ++r)
    {
        ui->fileTable->removeRow(r);
    }
    ui->ofnEdit->clear();
    // Clear the command string
    m_command = "";
}

void MainWindow::on_runBtn_clicked()
{
    if(!m_pdftkExists)
    {
        throwErrorMsg(PDFTK_NOT_FOUND, QString());
        return;
    }

    // Get the output file name and path
    QString ofName = ui->ofnEdit->text();
    if(!ofName.contains(".pdf"))
    {
        throwErrorMsg(OFNAME_WRONG, QString());
        return;
    }

    // Once the file paths are fine, start the work!
    setupCommand();

    // Run the command and then capture the output.
    QProcess proc;

    proc.start("pdftk " + m_command + "output " + ofName);
    proc.waitForFinished(-1);
    QString currStdout = proc.readAllStandardOutput();
    QString currStderr = proc.readAllStandardError();

    if(currStdout.contains("Error: ") || currStderr.contains("Error: ") || !currStderr.isEmpty())
    {
        throwErrorMsg(PDFTK_FAILED, currStderr);
    }
    else
    {
        throwErrorMsg(PDFTK_SUCCESS, QString());
    }

    m_command = "";
}

void MainWindow::on_hlpBtn_clicked()
{
    m_hlpDlg->show();
}

void MainWindow::on_advBtn_clicked()
{
    m_advDlg->exec();
}

void MainWindow::onThirdColumnClicked(QTableWidgetItem* it)
{
    if(it->column() != 3)
        return;

    // Rotate through the possible options
    if(it->text() == "None")
    {
        it->setText("90 deg CW");
    }
    else if(it->text() == "90 deg CW")
    {
        it->setText("90 deg CCW");
    }
    else if(it->text() == "90 deg CCW")
    {
        it->setText("180 deg");
    }
    else
    {
        it->setText("None");
    }
}

void MainWindow::on_bwsBtn_clicked()
{
    ui->ofnEdit->setText(QFileDialog::getSaveFileName(this, tr("Save File"),
                         QDir::homePath() + "/untitled.pdf", tr("*.pdf")));
}

void MainWindow::on_rmvBtn_clicked()
{
    QList<QTableWidgetItem*> itemsToRemove = ui->fileTable->selectedItems();
    // Get the unique set of rows to remove
    std::set<int, std::greater<int>> rowsToRemove;
    for(int i = 0; i < itemsToRemove.size(); ++i)
    {
        rowsToRemove.insert(itemsToRemove[i]->row());
    }

    // Remove the selected rows
    for(std::set<int, std::greater<int>>::iterator i = rowsToRemove.begin(); i != rowsToRemove.end(); ++i)
    {
        ui->fileTable->removeRow(*i);
    }
}

void MainWindow::on_upBtn_clicked()
{
    // TODO: Handle multiple rows

    QList<QTableWidgetItem *> selectedItems = ui->fileTable->selectedItems();
    // Sanity check
    if( selectedItems.size() < ui->fileTable->columnCount() )
    {
        return;
    }
    // If the row is already the top row do nothing
    if( selectedItems[0]->row() == 0 )
    {
        return;
    }

    // Swap the row with the one above
    int r = selectedItems[0]->row();
    QList<QTableWidgetItem *> src = takeRow( r );
    QList<QTableWidgetItem *> dst = takeRow( r - 1 );

    setRow( r, dst);
    setRow( r-1, src );
}

void MainWindow::on_dwnBtn_clicked()
{
    // TODO: Handle multiple rows

    QList<QTableWidgetItem *> selectedItems = ui->fileTable->selectedItems();
    // Sanity check
    if( selectedItems.size() < ui->fileTable->columnCount() )
    {
        return;
    }
    // If the row is already the last row do nothing
    if( (selectedItems[0]->row() + 1)  == ui->fileTable->rowCount() )
    {
        return;
    }

    // Swap the row with the one below
    int r = selectedItems[0]->row();
    QList<QTableWidgetItem *> src = takeRow( r );
    QList<QTableWidgetItem *> dst = takeRow( r + 1 );

    setRow( r, dst);
    setRow( r+1, src );
}

QList<QTableWidgetItem *> MainWindow::takeRow( int rowNum )
{
    QList<QTableWidgetItem *> ret;
    for( int i = 0; i < ui->fileTable->columnCount(); ++i )
    {
        ret.push_back( ui->fileTable->takeItem( rowNum, i ) );
    }
    return ret;
}

void MainWindow::setRow( int rowNum, QList<QTableWidgetItem *> items )
{
    for( int i = 0; i < items.size(); ++i )
    {
        ui->fileTable->setItem( rowNum, i, items[i] );
    }
}
