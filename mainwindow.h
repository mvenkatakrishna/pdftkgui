/**************************************************************************
 *  This file is part of PDFtkGUI.                                        *
 *                                                                        *
 *  PDFtkGUI is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  PDFtkGUI is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *                                                                        *
 *  You should have received a copy of the GNU General Public License     *
 *  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.       *
 **************************************************************************/

/*
 * Author: Mahesh Venkata Krishna (https://bitbucket.org/mvenkatakrishna/)
 */

/*
* Provides the main window of the project.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "helpdialog.h"
#include "advancedcommandsdialog.h"

#include <QMainWindow>
#include <QString>
#include <QList>
#include <QStringList>
#include <QTableWidgetItem>

// Various errors that can be generated. To be used in a message dialogbox.
enum errId
{
    PDFTK_NOT_FOUND,
    PDFTK_SUCCESS,
    PDFTK_FAILED,
    EMPTY_FILELIST,
    OFNAME_WRONG,
    TOO_FEW_TO_SHUFFLE,
    MORE_THAN_TWO_FILES,
    INVALID_ERR
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_addBtn_clicked();

    void on_clrBtn_clicked();

    void on_runBtn_clicked();

    void on_hlpBtn_clicked();

    void on_advBtn_clicked();

    // Function to handle the clicks on the rotation column
    void onThirdColumnClicked(QTableWidgetItem* it);

    void on_bwsBtn_clicked();

    void on_rmvBtn_clicked();

    void on_upBtn_clicked();

    void on_dwnBtn_clicked();

private:
    // Function to assemble the pdftk command based on all the input information
    void setupCommand();

    // Function to take out a complete row
    QList<QTableWidgetItem *> takeRow( int rowNum );
    // Function to set a complete row with the items passed to it
    void setRow( int rowNum, QList<QTableWidgetItem *> items );

    // Main ui
    Ui::MainWindow *ui;

    // Help and advanced usage dialogs
    HelpDialog * m_hlpDlg;
    // Advanced commands dialog
    AdvancedCommandsDialog * m_advDlg;

    // Flag to indicate whether pdftk exists in the system
    bool m_pdftkExists;
    // Command string to be run
    QString m_command;

};

#endif // MAINWINDOW_H
